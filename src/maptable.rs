pub use crate::table::{AutoId, ManualId, TableEntry, TableMeta};
use crate::table::{AutoIdTag, IdProvideTag, ManualIdTag, Table, TableId, WriteResult};
use std::collections::HashMap;
//should it be clonable????
/// Table that uses a map to get good speed random access without caring much for iteration
///
/// This is good when you only use IDs to look entries up directly, and iteration is unimportant

#[derive(Debug)]
pub struct MapTable<Id: TableId, T: 'static, InsertTag: IdProvideTag> {
    pub map: HashMap<Id, T>,
    next_id: Id,
    __phantom: std::marker::PhantomData<InsertTag>,
}

/// associated
impl<Id: TableId, T: 'static, InsertTag: IdProvideTag> MapTable<Id, T, InsertTag> {
    pub fn new() -> Self {
        Self {
            map: HashMap::new(),
            next_id: Id::start(),
            __phantom: std::marker::PhantomData,
        }
    }

    pub fn iter(&self) -> MapTableIterator<Id, T> {
        MapTableIterator {
            map_iter: self.map.iter(),
        }
    }
    pub fn iter_mut(&mut self) -> MapTableIteratorMut<Id, T> {
        MapTableIteratorMut {
            map_iter: self.map.iter_mut(),
        }
    }
}

/// Implement the table trait
impl<Id: TableId, T, InsertTag: IdProvideTag> Table for MapTable<Id, T, InsertTag> {
    type Id = Id;
    type TableItem = T;

    fn get(&self, id: Id) -> Option<&Self::TableItem> {
        self.map.get(&id)
    }
    fn get_mut(&mut self, id: Id) -> Option<&mut Self::TableItem> {
        self.map.get_mut(&id)
    }
    fn erase(&mut self, id: Self::Id) -> Option<Self::TableItem> {
        self.map.remove(&id)
    }
    fn len(&self) -> usize {
        self.map.len()
    }
    fn clear(&mut self) -> usize {
        let ret = self.len();
        self.map.clear();
        ret
    }
    fn meta(&self) -> TableMeta {
        TableMeta {
            name: String::from("unnamed maptable"),
        }
    }
    fn first(&self) -> Option<TableEntry<Self::Id, &Self::TableItem>> {
        self.iter().next()
    }
    fn first_mut(&mut self) -> Option<TableEntry<Self::Id, &mut Self::TableItem>> {
        self.iter_mut().next()
    }
}

/// implement the two traits that enable insertion
impl<Id: TableId, T> AutoId for MapTable<Id, T, AutoIdTag> {
    fn insert(&mut self, data: T) -> TableEntry<Id, &mut T> {
        let id = self.next_id;
        self.next_id = self.next_id.next();

        self.map.insert(id, data);

        TableEntry {
            id,
            data: self.map.get_mut(&id).unwrap(),
        }
    }
}

impl<Id: TableId, T> ManualId for MapTable<Id, T, ManualIdTag> {
    fn insert(&mut self, id: Id, data: T) -> WriteResult<Self::Id, Self::TableItem> {
        match self.get(id) {
            Some(_) => WriteResult::Rejected(data),
            None => {
                self.map.insert(id, data);

                WriteResult::Written(TableEntry {
                    id,
                    data: self.map.get_mut(&id).unwrap(),
                })
            }
        }
    }
}

/// Implement into iterator for this table
impl<Id: TableId, T, InsertTag: IdProvideTag> IntoIterator for MapTable<Id, T, InsertTag> {
    type Item = TableEntry<Id, T>;
    type IntoIter = MapTableIntoIterator<Id, T>;

    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            map_iter: self.map.into_iter(),
        }
    }
}

impl<'a, Id: TableId, T, InsertTag: IdProvideTag> IntoIterator for &'a MapTable<Id, T, InsertTag> {
    type Item = TableEntry<Id, &'a T>;
    type IntoIter = MapTableIterator<'a, Id, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, Id: TableId, T, InsertTag: IdProvideTag> IntoIterator
    for &'a mut MapTable<Id, T, InsertTag>
{
    type Item = TableEntry<Id, &'a mut T>;
    type IntoIter = MapTableIteratorMut<'a, Id, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

/// Iterator types and their implementations
pub struct MapTableIntoIterator<Id, T> {
    map_iter: std::collections::hash_map::IntoIter<Id, T>,
}

impl<Id: TableId, T> Iterator for MapTableIntoIterator<Id, T> {
    type Item = TableEntry<Id, T>;

    fn next(&mut self) -> Option<Self::Item> {
        self.map_iter
            .next()
            .map(|e| Self::Item { id: e.0, data: e.1 })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.map_iter.size_hint()
    }

    fn count(self) -> usize {
        self.map_iter.count()
    }
}
pub struct MapTableIterator<'a, Id, T> {
    map_iter: std::collections::hash_map::Iter<'a, Id, T>,
}

impl<'a, Id: TableId, T> Iterator for MapTableIterator<'a, Id, T> {
    type Item = TableEntry<Id, &'a T>;

    fn next(&mut self) -> Option<Self::Item> {
        self.map_iter.next().map(|e| Self::Item {
            id: *e.0,
            data: e.1,
        })
    }
}
pub struct MapTableIteratorMut<'a, Id, T> {
    map_iter: std::collections::hash_map::IterMut<'a, Id, T>,
}

impl<'a, Id: TableId, T> Iterator for MapTableIteratorMut<'a, Id, T> {
    type Item = TableEntry<Id, &'a mut T>;

    fn next(&mut self) -> Option<Self::Item> {
        self.map_iter.next().map(|e| Self::Item {
            id: *e.0,
            data: e.1,
        })
    }
}
