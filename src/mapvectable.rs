pub use crate::table::{AutoId, ManualId, TableEntry, TableMeta};
use crate::table::{AutoIdTag, IdProvideTag, ManualIdTag, Ordered, Table, TableId, WriteResult};
use std::collections::HashMap;
//should it be clonable????
/// Table that uses both a vec and a map to get good speed for both iteration and random access
///
/// This is a good all-rounder table. The drawbacks are more memory usage and a possible extra cache hit when
/// doing random lookup.
#[derive(Debug)]
pub struct MapVecTable<Id: TableId, T, InsertTag: IdProvideTag> {
    pub ids: Vec<Id>,
    pub data: Vec<T>,
    pub index_map: HashMap<Id, usize>,
    next_id: Id,
    __phantom: std::marker::PhantomData<InsertTag>,
}

/// associated
impl<Id: TableId, T, InsertTag: IdProvideTag> MapVecTable<Id, T, InsertTag> {
    pub fn new() -> Self {
        Self {
            ids: vec![],
            data: vec![],
            index_map: HashMap::new(),
            next_id: Id::start(),
            __phantom: std::marker::PhantomData,
        }
    }

    pub fn iter(&self) -> MapVecTableIterator<Id, T> {
        MapVecTableIterator {
            id_iter: self.ids.iter(),
            data_iter: self.data.iter(),
        }
    }
    pub fn iter_mut(&mut self) -> MapVecTableIteratorMut<Id, T> {
        MapVecTableIteratorMut {
            id_iter: self.ids.iter_mut(),
            data_iter: self.data.iter_mut(),
        }
    }
}

/// Implement the table trait
impl<Id: TableId, T: 'static, InsertTag: IdProvideTag> Table for MapVecTable<Id, T, InsertTag> {
    type Id = Id;
    type TableItem = T;

    fn get(&self, id: Id) -> Option<&Self::TableItem> {
        self.index_map.get(&id).map(|i| &self.data[*i])
    }
    fn get_mut(&mut self, id: Id) -> Option<&mut Self::TableItem> {
        let index = self.index_map.get(&id);

        match index {
            Some(index) => self.data.get_mut(*index),
            None => None,
        }
    }
    fn erase(&mut self, id: Self::Id) -> Option<Self::TableItem> {
        let index_to_delete = match self.index_map.get(&id) {
            Some(i) => *i,
            None => return None,
        };

        self.ids.remove(index_to_delete);
        let removed = self.data.remove(index_to_delete);
        self.index_map.remove(&id);

        for index in self
            .index_map
            .values_mut()
            .filter(|i| **i >= index_to_delete)
        {
            *index -= 1;
        }
        Some(removed)
    }
    fn len(&self) -> usize {
        self.ids.len()
    }
    fn clear(&mut self) -> usize {
        let ret = self.len();
        self.ids.clear();
        self.data.clear();
        ret
    }
    fn meta(&self) -> TableMeta {
        TableMeta {
            name: String::from("unnamed mapvectable"),
        }
    }
    fn first(&self) -> Option<TableEntry<Self::Id, &Self::TableItem>> {
        self.iter().next()
    }
    fn first_mut(&mut self) -> Option<TableEntry<Self::Id, &mut Self::TableItem>> {
        self.iter_mut().next()
    }
}

/// implement the two traits that enable insertion
impl<Id: TableId, T: 'static> AutoId for MapVecTable<Id, T, AutoIdTag> {
    fn insert(&mut self, data: T) -> TableEntry<Id, &mut T> {
        let id = self.next_id;
        self.next_id = self.next_id.next();

        self.ids.push(id);
        self.data.push(data);
        self.index_map.insert(id, self.ids.len() - 1);

        TableEntry {
            id,
            data: self.data.last_mut().unwrap(),
        }
    }
}

impl<Id: TableId, T: 'static> ManualId for MapVecTable<Id, T, ManualIdTag> {
    fn insert(&mut self, id: Id, data: T) -> WriteResult<Self::Id, Self::TableItem> {
        match self.get(id) {
            Some(_) => WriteResult::Rejected(data),
            None => {
                self.ids.push(id);
                self.data.push(data);
                self.index_map.insert(id, self.ids.len() - 1);

                WriteResult::Written(TableEntry {
                    id,
                    data: self.data.last_mut().unwrap(),
                })
            }
        }
    }
}

/// This table has ordered IDs
impl<Id: TableId, T, InsertTag: IdProvideTag> Ordered for MapVecTable<Id, T, InsertTag> {}

impl<'a, Id: TableId, T, InsertTag: IdProvideTag> Ordered for &'a MapVecTable<Id, T, InsertTag> {}

impl<'a, Id: TableId, T, InsertTag: IdProvideTag> Ordered
    for &'a mut MapVecTable<Id, T, InsertTag>
{
}

/// Implement into iterator for this table
impl<Id: TableId, T, InsertTag: IdProvideTag> IntoIterator for MapVecTable<Id, T, InsertTag> {
    type Item = TableEntry<Id, T>;
    type IntoIter = MapVecTableIntoIterator<Id, T>;

    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            id_iter: self.ids.into_iter(),
            data_iter: self.data.into_iter(),
        }
    }
}

impl<'a, Id: TableId, T, InsertTag: IdProvideTag> IntoIterator
    for &'a MapVecTable<Id, T, InsertTag>
{
    type Item = TableEntry<Id, &'a T>;
    type IntoIter = MapVecTableIterator<'a, Id, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, Id: TableId, T, InsertTag: IdProvideTag> IntoIterator
    for &'a mut MapVecTable<Id, T, InsertTag>
{
    type Item = TableEntry<Id, &'a mut T>;
    type IntoIter = MapVecTableIteratorMut<'a, Id, T>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

/// Iterator types and their implementations
pub struct MapVecTableIntoIterator<Id, T> {
    id_iter: std::vec::IntoIter<Id>,
    data_iter: std::vec::IntoIter<T>,
}

impl<Id: TableId, T> Iterator for MapVecTableIntoIterator<Id, T> {
    type Item = TableEntry<Id, T>;

    fn next(&mut self) -> Option<Self::Item> {
        let next_id = self.id_iter.next();
        let next_data = self.data_iter.next();

        if let Some(id) = next_id {
            Some(TableEntry {
                id,
                data: next_data.unwrap(),
            })
        } else {
            None
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.id_iter.size_hint()
    }

    fn count(self) -> usize {
        self.id_iter.count()
    }
}

pub struct MapVecTableIterator<'a, Id, T> {
    pub id_iter: std::slice::Iter<'a, Id>,
    pub data_iter: std::slice::Iter<'a, T>,
}

impl<'a, Id: TableId, T> Iterator for MapVecTableIterator<'a, Id, T> {
    type Item = TableEntry<Id, &'a T>;

    fn next(&mut self) -> Option<Self::Item> {
        let next_id = self.id_iter.next();
        let next_data = self.data_iter.next();

        if let Some(id) = next_id {
            Some(TableEntry {
                id: *id,
                data: next_data.unwrap(),
            })
        } else {
            None
        }
    }
}

pub struct MapVecTableIteratorMut<'a, Id, T> {
    pub id_iter: std::slice::IterMut<'a, Id>,
    pub data_iter: std::slice::IterMut<'a, T>,
}

impl<'a, Id: TableId, T> Iterator for MapVecTableIteratorMut<'a, Id, T> {
    type Item = TableEntry<Id, &'a mut T>;

    fn next(&mut self) -> Option<Self::Item> {
        let next_id = self.id_iter.next();
        let next_data = self.data_iter.next();

        if let Some(id) = next_id {
            Some(TableEntry {
                id: *id,
                data: next_data.unwrap(),
            })
        } else {
            None
        }
    }
}
