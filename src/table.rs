use std::hash::Hash;
use std::num::NonZeroUsize;

pub struct TableEntry<Id: TableId, T> {
    pub id: Id,
    pub data: T,
}

pub trait TableId: Ord + PartialOrd + Eq + PartialEq + Copy + Hash {
    fn next(&self) -> Self;
    fn start() -> Self;
}

impl TableId for usize {
    fn next(&self) -> Self {
        self + 1
    }
    fn start() -> Self {
        1
    }
}

impl TableId for NonZeroUsize {
    fn next(&self) -> Self {
        unsafe { NonZeroUsize::new_unchecked(self.get() + 1) }
    }
    fn start() -> Self {
        unsafe { NonZeroUsize::new_unchecked(1) }
    }
}

pub struct TableMeta {
    pub name: String,
}

/// A key->value store that maps a unique ID to an item
pub trait Table {
    type Id: TableId;
    type TableItem: 'static;

    //get
    fn get(&self, id: Self::Id) -> Option<&Self::TableItem>;
    fn get_mut(&mut self, id: Self::Id) -> Option<&mut Self::TableItem>;
    fn get_entry(&self, id: Self::Id) -> Option<TableEntry<Self::Id, &Self::TableItem>> {
        self.get(id).map(|e| TableEntry { id, data: e })
    }
    fn get_entry_mut(
        &mut self,
        id: Self::Id,
    ) -> Option<TableEntry<Self::Id, &mut Self::TableItem>> {
        self.get_mut(id).map(|e| TableEntry { id, data: e })
    }
    //first
    fn first(&self) -> Option<TableEntry<Self::Id, &Self::TableItem>>;
    fn first_mut(&mut self) -> Option<TableEntry<Self::Id, &mut Self::TableItem>>;
    //auto: get_any. use iter

    //erase
    fn erase(&mut self, id: Self::Id) -> Option<Self::TableItem>;
    fn erase_if(&mut self, id: Option<Self::Id>) -> Option<Self::TableItem> {
        match id {
            Some(id) => self.erase(id),
            None => None,
        }
    }
    //fn erase_if(&mut self, f: impl Fn(TableEntry<Self::Id, &Self::TableItem>)) -> usize;
    //fn drain_if(
    //    &mut self,
    //    f: impl Fn(TableEntry<Self::Id, &Self::TableItem>) -> bool,
    //) -> Vec<TableEntry<Self::Id, Self::TableItem>>;

    fn len(&self) -> usize; //add default impl that uses iterators?
    fn clear(&mut self) -> usize;
    //fn drain_all(&mut self) -> Vec<TableEntry<Self::Id, Self::TableItem>> {
    //    self.drain_if(|_| true)
    //}

    fn has(&self, id: Self::Id) -> bool {
        self.get(id).is_some()
    }

    //mapping
    fn map_to_data(&self, id: Option<Self::Id>) -> Option<&Self::TableItem> {
        id.and_then(|id| self.get(id))
    }
    fn map_to_data_mut(&self, id: Option<Self::Id>) -> Option<&Self::TableItem> {
        id.and_then(|id| self.get(id))
    }
    fn map_with<T>(
        &self,
        id: Option<Self::Id>,
        f: impl Fn(TableEntry<Self::Id, &Self::TableItem>) -> T,
    ) -> Option<T> {
        id.and_then(|id| self.get_entry(id)).map(|e| f(e))
    }
    fn map_with_mut<T>(
        &mut self,
        id: Option<Self::Id>,
        f: impl Fn(TableEntry<Self::Id, &mut Self::TableItem>) -> T,
    ) -> Option<T> {
        id.and_then(|id| self.get_entry_mut(id)).map(|e| f(e))
    }
    //meta
    fn meta(&self) -> TableMeta;
}

/// A table that is iterable with IDs in incremental order should implement this trait
pub trait Ordered {}

pub trait IdProvideTag {}
pub struct AutoIdTag();
impl IdProvideTag for AutoIdTag {}
pub struct ManualIdTag();
impl IdProvideTag for ManualIdTag {}

pub enum WriteResult<'a, Id: TableId, TableItem> {
    Written(TableEntry<Id, &'a mut TableItem>),
    Rejected(TableItem),
}
/// Will provide the ID of entries automatically. IDs are not provided when inserting.
pub trait AutoId: Table {
    /// cannot fail. always inserts
    fn insert(&mut self, data: Self::TableItem) -> TableEntry<Self::Id, &mut Self::TableItem>;

    /// if id exists, update data of that entry
    /// if id does not exist, not allowed to set. return rejected item
    fn set(
        &mut self,
        id: Self::Id,
        data: Self::TableItem,
    ) -> WriteResult<Self::Id, Self::TableItem> {
        match self.get_mut(id) {
            Some(existing) => {
                *existing = data;
                WriteResult::Written(TableEntry { id, data: existing })
            }
            None => WriteResult::Rejected(data),
        }
    }
}

/// IDs have to be manually given on insertion
pub trait ManualId: Table {
    /// inserts if the Id does not already exist.
    /// if it does exist, the insertion is rejected
    fn insert(
        &mut self,
        id: Self::Id,
        data: Self::TableItem,
    ) -> WriteResult<Self::Id, Self::TableItem>;

    /// if id exists, update data of that entry
    /// if id does not exist, create it. cannot fail, always returns entry
    fn set(
        &mut self,
        id: Self::Id,
        data: Self::TableItem,
    ) -> TableEntry<Self::Id, &mut Self::TableItem> {
        if !self.has(id) {
            match self.insert(id, data) {
                WriteResult::Written(entry) => return entry,
                _ => unreachable!(),
            }
        } else {
            let found = self.get_entry_mut(id);
            match found {
                Some(existing) => {
                    *existing.data = data;
                    return existing;
                }
                _ => unreachable!(),
            }
        }
    }
}
